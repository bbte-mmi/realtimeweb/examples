import { baseApiUrl } from '../constants';
import { UserCreateDto, UserDto, UserUpdateDto } from '../dto/user.dto';

export const findAllUsers = async (): Promise<UserDto[]> => {
  const response = await fetch(`${baseApiUrl}/users`);
  const entities = (await response.json()) as UserDto[];
  return entities;
};

export const findUserById = async (userId: string): Promise<UserDto | undefined> => {
  const response = await fetch(`${baseApiUrl}/users/${userId}`);
  if (response.status === 404) {
    return undefined;
  }
  const entity = (await response.json()) as UserDto;
  return entity;
};

export const createUser = async (user: UserCreateDto): Promise<UserDto> => {
  const response = await fetch(`${baseApiUrl}/users`, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(user),
  });

  if (response.status >= 400) {
    const { messages } = await response.json();
    throw new Error(messages || 'Unknown error');
  }

  const entity = (await response.json()) as UserDto;
  return entity;
};

export const updateUser = async (userId: string, user: UserUpdateDto): Promise<UserDto> => {
  const response = await fetch(`${baseApiUrl}/users/${userId}`, {
    method: 'PATCH',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(user),
  });

  if (response.status >= 400) {
    const { messages } = await response.json();
    throw new Error(messages || 'Unknown error');
  }

  const entity = (await response.json()) as UserDto;
  return entity;
};

export const deleteUser = async (userId: string): Promise<void> => {
  await fetch(`${baseApiUrl}/users/${userId}`, {
    method: 'DELETE',
  });
};
