let sendConnection;    // küldő fél kapcsolata

// STOMP kliens kapcsolati objektum deklarálása
let client;
let connectionId;
let stream;

const connectPeers = () => {
  // küldő kapcsolat megnyitása
  sendConnection = new RTCPeerConnection();

  // hozzákötjük a trackekhez az RTC kapcsolatot
  stream.getTracks().forEach((track) => {
    sendConnection.addTrack(track, stream);
  });

  // közös ICE jelölteket biztosítunk
  sendConnection.onicecandidate = (e) => {
    if (e.candidate) {
      const headers = { 'Content-Type': 'application/json', 'auto-delete': true };
      client.send(`/queue/webrtc-videostreaming-sender-icecandidate-${connectionId}`, headers, JSON.stringify(e.candidate));
    }
  };

  // P2P kapcsolat létesítése
  // 1. a küldő fél ajánlatot generál (itt kap ICE nevet)
  sendConnection.createOffer()
    // 2. a küldő fél beállítja saját leírását
    .then((offer) => sendConnection.setLocalDescription(offer))
    // 3. beírjuk az ajánlatot a text fieldbe, a fogadófélnek ismernie kell
    .then(() => {
      document.getElementById('status').innerText = 'Offer generated, waiting for answer';
      // elküldjük az offert az offer topicra
      const message = {
        offer: sendConnection.localDescription,
        connectionId,
      };
      client.send('/queue/webrtc-videostreaming-offer', { 'Content-Type': 'application/json' }, JSON.stringify(message));
    })
    .catch((error) => console.error(`Unable to establish P2P communication: ${error}`));
};

const onIcecandidate = (message) => {
  const icecandidate = JSON.parse(message.body);
  sendConnection.addIceCandidate(icecandidate);
};

const onAnswer = (message) => {
  const answer = JSON.parse(message.body);

  // a küldő fél a beérkező címét beállítja a fogadó fél kimenő címére
  sendConnection.setRemoteDescription(answer)
    // siker
    .then(() => { document.getElementById('status').innerHTML = 'Connected!'; })
    .catch((error) => console.error(`Unable to establish P2P communication: ${error}`));
};

window.onload = () => {
  // egyedi azonosító generálása
  connectionId = btoa(crypto.getRandomValues(new Uint8Array(2)));

  // Kapcsolat megnyitása
  // RabbitMQ STOMP-over-WebSocket kapcsolata
  // port = 15674, subprotocol = STOMP
  /* eslint-disable no-restricted-globals */
  /* eslint-disable no-undef */
  client = Stomp.client(`ws://${location.hostname}:15674/ws`);
  client.debug = null;

  // sikeres kapcsolat esetén
  const onConnect = () => {
    // elkérjük a webcam streamjét
    navigator.mediaDevices.getUserMedia({ audio: false, video: true })
      .then((newStream) => {
        stream = newStream;
        // a lokális stream bemeneteként állítjuk
        const localVideo = document.getElementById('local-video');
        localVideo.srcObject = stream;

        connectPeers();
      })
      .catch((err) => { console.error(err); });

    // figyelünk a dedikált answer nevű queue-ra
    client.subscribe(`/queue/webrtc-videostreaming-answer-${connectionId}`, onAnswer, { 'auto-delete': true });
    // figyelünk a dedikált icecandidate-ekre
    client.subscribe(`/queue/webrtc-videostreaming-receiver-icecandidate-${connectionId}`, onIcecandidate, { 'auto-delete': true });
  };

  // STOMP kapcsolat
  client.connect('guest', 'guest', onConnect, console.error, '/');
};
