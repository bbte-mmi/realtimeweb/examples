import ChannelService from './ChannelService';
import MessageService from './MessageService';
import UserService from './UserService';

const services = [ChannelService, MessageService, UserService];
export default services;
