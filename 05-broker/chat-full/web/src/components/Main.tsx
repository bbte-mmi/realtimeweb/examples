import React, { useContext, useEffect, useState } from 'react';
import { fetchChannels } from '../api/ChannelService';
import UserContext from '../context/UserContext';
import { ChannelDto } from '../dto/ChannelDto';
import { UserDto } from '../dto/UserDto';
import ChannelComponent from './Channel';
import Channels from './Channels';

export type MainProps = {
  logout: () => unknown;
};

export default function Main(props: MainProps) {
  const user = useContext<UserDto>(UserContext);
  const [channels, setChannels] = useState<ChannelDto[]>(null);
  const [selectedChannel, setSelectedChannel] = useState<ChannelDto>(null);

  useEffect(() => {
    (async () => {
      const newChannels = await fetchChannels();
      setChannels(newChannels);
    })();
  }, []);

  return (
    <>
      <div id="user">
        &#128100; <b>{user.name}</b> (<i>{user.id}</i>)&nbsp;
        <span className="link" role="link" tabIndex={0} onKeyUp={props.logout} onClick={props.logout}>
          Log out
        </span>
      </div>
      <div id="main">
        <Channels
          channels={channels}
          setChannels={setChannels}
          selectedChannel={selectedChannel}
          setSelectedChannel={setSelectedChannel}
        />
        <ChannelComponent channel={selectedChannel} />
      </div>
    </>
  );
}
