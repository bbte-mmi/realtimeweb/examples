import { Inject, Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import Channel from '../model/Channel';

@Injectable()
export default class ChannelService {
  @Inject('ChannelRepository')
  private readonly channelRepository: Repository<Channel>;

  findAllChannels(): Promise<Channel[]> {
    return this.channelRepository.find();
  }

  findChannelById(channelId: number): Promise<Channel> {
    return this.channelRepository.findOneBy({ id: channelId });
  }

  createChannel(channel: Partial<Channel>): Promise<Channel> {
    return this.channelRepository.save(channel, { reload: true });
  }

  async deleteChannelById(channelId: number): Promise<boolean> {
    const result = await this.channelRepository.delete({ id: channelId });
    return result.affected > 0;
  }
}
