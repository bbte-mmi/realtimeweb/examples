type ErrorMessageProps = {
  message: string;
};

export default function ErrorMessage(props: ErrorMessageProps) {
  if (!props.message) {
    return null;
  }

  return (
    <div className="row">
      <div className="col col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div className="alert alert-dismissible alert-danger">
          <i>ERROR:</i> {props.message}
        </div>
      </div>
    </div>
  );
}
