import Channel from './Channel';
import Message from './Message';
import User from './User';

const models = [Channel, Message, User];
export default models;
