import { useCallback, useEffect, useRef, useState } from 'react';
import { createRoot } from 'react-dom/client';

import './style.css';

function CaptureStill() {
  const canvasRef = useRef<HTMLCanvasElement>(null);
  const localVideoRef = useRef<HTMLVideoElement>(null);
  const [pictureData, setPictureData] = useState<string>();

  useEffect(() => {
    const constraints = {
      audio: false,
      video: {
        width: 320,
      },
    };

    // elkérjük a webcam streamjét
    navigator.mediaDevices.getUserMedia(constraints).then((stream) => {
      if (!localVideoRef?.current) {
        return;
      }
      // a <video> elem bemenete a lokális streamünk
      localVideoRef.current.srcObject = stream;
      // "elindítjuk" a streamelést (nem minden böngészőnél szükséges)
      localVideoRef.current.play();
    });
  }, [localVideoRef]);

  const captureStill = useCallback(() => {
    if (!canvasRef?.current || !localVideoRef?.current) {
      return;
    }

    const context = canvasRef.current.getContext('2d');
    canvasRef.current.width = localVideoRef.current.videoWidth;
    canvasRef.current.height = localVideoRef.current.videoHeight;

    // a canvasre rajzoljuk a video jelenlegi tartalmát
    // a drawImage támogatja a HTMLVideoElement típusú paramétert
    context?.drawImage(localVideoRef.current, 0, 0);

    // a canvasből base64 képadatot generálunk
    const data = canvasRef.current.toDataURL('image/png');
    setPictureData(data);
  }, []);

  return (
    <>
      <h1>Capture still with MediaStream</h1>

      <input type="button" onClick={captureStill} value="Capture image" />

      <div className="video-container">
        {/* webcam bemenet */}
        <video autoPlay className="local-video" ref={localVideoRef} muted />
        {/* canvas amire rajzolunk */}
        <canvas ref={canvasRef} />
        {/* üres img ahova mentünk képeket a webcamtől */}
        <img src={pictureData} alt="The screen capture will appear in this box." />
      </div>

      <div className="video-captions">
        <span>My Webcam</span>
        <span>Canvas still</span>
        <span>Img still</span>
      </div>
    </>
  );
}

const root = document.getElementById('root') as HTMLDivElement;
createRoot(root).render(<CaptureStill />);
