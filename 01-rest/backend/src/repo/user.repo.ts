import { Db } from 'mongodb';
import { User } from '../model/user.model';
import { Repository } from './base.repo';

/**
 * Konkrét adatelérési objektum.
 * Tartalmazhat entitás-specifikus lekérdezéseket
 */
export class UserRepository extends Repository<User> {
  // keresés név alapján
  findByName(fullName: string): Promise<User[]> {
    return this.collection.find<User>({ fullName }, {}).toArray();
  }
}

/**
 * Alapértelmezett példány
 */
export default function getUserRepo(db: Db) {
  const userCollection = db.collection<User>('users');
  const userRepo = new UserRepository(userCollection);
  return userRepo;
}
