import express from 'express';

import { handleError } from './middleware/errorhandling';
import getBlogPostRepo from './repo/blogpost.repo';
import getUserRepo from './repo/user.repo';
import getApiRoutes from './route/base.route';
import { port } from './util/config';
import { getMongoDb } from './util/mongo';

(async () => {
  // adatbázis csatlakozás
  const db = await getMongoDb();
  const blogPostRepo = getBlogPostRepo(db);
  const userRepo = getUserRepo(db);

  // Express alkalmazás felépítése
  const app = express();

  // az API-t bekötjük a /api... endpointra
  app.use('/api', getApiRoutes(blogPostRepo, userRepo));

  // utolsó middleware = hibakezelés
  app.use(handleError);

  app.listen(port, () => {
    console.log(`Server listening on http://localhost:${port}/ ...`);
  });
})();
