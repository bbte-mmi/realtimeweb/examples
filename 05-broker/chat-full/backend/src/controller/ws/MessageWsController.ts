import { Inject } from '@nestjs/common';
import { OnGatewayDisconnect, SubscribeMessage, WebSocketGateway } from '@nestjs/websockets';
import { Channel } from 'amqplib';
import { Socket } from 'socket.io';
import { MessageDto, WsMessageCreationDto } from '../../dto/MessageDto';
import * as messageMapper from '../../mapper/MessageMapper';
import MessageService from '../../service/MessageService';

type ExtendedSocket = Socket & {
  consumerTag: string;
};

@WebSocketGateway({
  cors: {
    origin: '*',
  },
})
export default class MessageWsController implements OnGatewayDisconnect<ExtendedSocket> {
  @Inject()
  messageService: MessageService;

  @Inject('AMQP_CHANNEL')
  amqpChannel: Channel;

  @SubscribeMessage('message')
  async onMessage(socket: ExtendedSocket, dto: WsMessageCreationDto): Promise<void> {
    console.log(`Socket ${socket.id} message ${dto.channelId} => ${dto.text}`);
    const model = messageMapper.wsDtoToModel(dto);
    const message = await this.messageService.createMessage(model);
    const messageDto = messageMapper.modelToDto(message);

    const payload = Buffer.from(JSON.stringify(messageDto));
    this.amqpChannel.publish('chat', `channel.${dto.channelId}`, payload);
  }

  handleDisconnect(socket: ExtendedSocket) {
    if (socket.consumerTag) {
      this.amqpChannel.cancel(socket.consumerTag);
    }
  }

  @SubscribeMessage('join')
  async onJoin(socket: ExtendedSocket, channelId: number): Promise<void> {
    if (socket.consumerTag) {
      await this.amqpChannel.cancel(socket.consumerTag);
    }

    const { queue } = await this.amqpChannel.assertQueue('', { exclusive: true, autoDelete: true });
    await this.amqpChannel.bindQueue(queue, 'chat', `channel.${channelId}`);
    const { consumerTag } = await this.amqpChannel.consume(queue, (payload) => {
      const message = JSON.parse(payload.content.toString()) as MessageDto;
      socket.emit('message', message);
      this.amqpChannel.ack(payload);
    });
    socket.consumerTag = consumerTag;
  }
}
