import { FormEvent, useCallback, useEffect, useMemo, useState } from 'react';
import { createRoot } from 'react-dom/client';
import { Socket, io } from 'socket.io-client';

import './style.css';

//
// socket io DTOs
//

type BroadcastMessage = {
  clientId: string;
  text: string;
  time: string;
};

interface ServerToClientEvents {
  message: (message: BroadcastMessage) => void;
  assignid: (id: string) => void;
}

interface ClientToServerEvents {
  message: (message: BroadcastMessage) => void;
}

function ChatSocketIo() {
  const [clientId, setClientId] = useState('unknown');
  const [text, setText] = useState<string>('');
  const [messages, setMessages] = useState<Array<BroadcastMessage>>([]);

  // Kapcsolat megnyitása a dedikált WS felé
  const socket = useMemo<Socket<ServerToClientEvents, ClientToServerEvents>>(() => io('http://localhost:8080/'), []);

  useEffect(() => {
    // ha üzenetet kapunk a szervertől
    socket.on('message', (message: BroadcastMessage) => {
      setMessages((m) => [...m, message]);
    });

    socket.on('assignid', (c: string) => setClientId(c));

    // unmountkor zárjuk a socket.io kapcsolatot
    return () => {
      console.log('Disconnecting from WebSocket');
      socket.close();
    };
  }, []);

  // gombnyomás az üzenetnél
  const sendMessage = useCallback(
    (event: FormEvent) => {
      // szinkron leadás elkerülése
      event.preventDefault();

      // üzenetet küldünk
      const message: BroadcastMessage = {
        clientId,
        text,
        time: new Date().toISOString(),
      };

      // socket.io-val küldjük az üzenetet
      socket.emit('message', message);

      // a szöveges bemenetet ürítjük
      setMessages((m) => [...m, message]);
      setText('');
    },
    [clientId, text, messages],
  );

  return (
    <>
      <h1>Chat with socket.io</h1>

      <div>
        <b>Status:</b> Connected as <code>{clientId}</code>
      </div>

      <div id="messages">
        {messages.map((message) => (
          <div key={`${message.clientId}-${message.time}`} className={message.clientId === clientId ? 'me' : 'them'}>
            <span className="name">{message.clientId}:&nbsp;</span>
            <span className="msg">{message.text}</span>
          </div>
        ))}
      </div>

      {/* <!-- form, amin keresztül üzenetet küldhetünk --> */}
      <form onSubmit={sendMessage}>
        <input
          style={{ textAlign: 'right' }}
          type="text"
          value={text}
          onChange={(event) => setText(event.target.value)}
          placeholder="Type message and press ENTER..."
        />
      </form>
    </>
  );
}

const root = document.getElementById('root') as HTMLDivElement;
createRoot(root).render(<ChatSocketIo />);
