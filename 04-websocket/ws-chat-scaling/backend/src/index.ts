import { createClient } from 'redis';
import WebSocket, { WebSocketServer } from 'ws';

/**
 * Csatorna neve, amin keresztül a Redis-en küldjük az üzeneteket
 */
const redisChannelName = process.env.REDIS_CHANNEL_NAME || 'realtimeweb-chat';

(async () => {
  //
  // két Redis kapcsolat (egy küldésre, egy fogadásra)
  //
  const pubClient = createClient({ url: 'redis://localhost:6379' });
  const subClient = pubClient.duplicate();

  await Promise.all([pubClient.connect(), subClient.connect()]);

  // WS szerver
  const port = Number(process.env.PORT) || 8080;
  const wss = new WebSocketServer({ port });

  wss.on('listening', () => {
    console.log(`Server listening on http://localhost:${port}/ ...`);
  });

  //
  // redis üzenet másik példányoktól (channelen)
  // továbbítjuk mindenkinek, aki ide van csatlakozva
  //
  subClient.subscribe(redisChannelName, (message) => {
    wss.clients.forEach((client) => {
      if (client.readyState === WebSocket.OPEN) {
        client.send(message);
      }
    });
  });

  // websocket csatlakozásának lekezelése
  wss.on('connection', (ws, req) => {
    // egyedi azonosító kibányászása a request URL-ből
    const url = new URL(`http://localhost:8080${req.url}`);
    const id = url.searchParams.get('clientId');

    console.log(`[${id}] connected from ${req.socket.remoteAddress}`);

    //
    // saját kliens esetén nem továbbítjuk
    // a helyi szerveren, hanem kiküldjük a Redisre
    //
    ws.on('message', (message: string) => {
      console.log(`[${id}] sent '${message}', relaying`);
      pubClient.publish(redisChannelName, message);
    });

    // socket zárásakor töröljük az aktív kliensek listájából
    ws.on('close', () => {
      console.log(`[${id}] closed connection`);
    });
  });
})();
