import { ValidationError, validate } from 'class-validator';
import { NextFunction, Request, RequestHandler, Response } from 'express';
import { ObjectId } from 'mongodb';

import { BadRequestError } from './errorhandling';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type Constructor<T> = { new (...args: any[]): T };

export const bodyValidator = <T extends object>(BodyType: Constructor<T>): RequestHandler => {
  return async (req: Request, res: Response, next: NextFunction) => {
    try {
      const bodyObject = new BodyType(req.body);
      const errors = await validate(bodyObject);
      if (errors.length > 0) {
        const errorsMessages = errors.map((error: ValidationError) => Object.values(error.constraints ?? {})).flat();
        next(new BadRequestError(errorsMessages));
        return;
      }
      req.body = bodyObject;
      next();
    } catch (error) {
      next(new BadRequestError(`Could not parse body: ${error}`));
    }
  };
};

export const mongoIdValidator = (paramKey = 'id'): RequestHandler => {
  return (req: Request, res: Response, next: NextFunction) => {
    const objectIdStr = req.params[paramKey];
    if (!ObjectId.isValid(objectIdStr)) {
      next(new BadRequestError(`${objectIdStr} is not a valid ObjectId`));
      return;
    }
    next();
  };
};
