let sendConnection;    // küldő fél kapcsolata
let sendChannel;       // küldő fél folyama

// kapcsolathoz szükséges üzenet
const offerMessage = {
  offer: undefined,
  icecandidates: [],
};

const connectPeers = () => {
  // küldő kapcsolat megnyitása
  sendConnection = new RTCPeerConnection();
  // adatfolyam megnyitása
  sendChannel = sendConnection.createDataChannel('sendChannel');

  // közös ICE jelölteket biztosítunk
  sendConnection.onicecandidate = (e) => {
    if (e.candidate) {
      offerMessage.icecandidates.push(e.candidate);
      document.getElementById('offer').value = JSON.stringify(offerMessage);
    }
  };

  // P2P kapcsolat létesítése
  // 1. a küldő fél ajánlatot generál (itt kap ICE nevet)
  sendConnection.createOffer()
    // 2. a küldő fél beállítja saját leírását
    .then((offer) => {
      offerMessage.offer = offer;
      sendConnection.setLocalDescription(offer);
    })
    // 3. beírjuk az ajánlatot a text fieldbe, a fogadófélnek ismernie kell
    .then(() => {
      document.getElementById('status').innerText = 'Offer generated, waiting for answer';
      document.getElementById('offer').value = JSON.stringify(offerMessage);
    })
    .catch((error) => console.error(`Unable to establish P2P communication: ${error}`));
};

const onAnswer = () => {
  // kiolvassuk a másik fél ajánlatát a szöveges bemenetből
  const answerMessage = JSON.parse(document.getElementById('answer').value);

  // a küldő fél a beérkező címét beállítja a fogadó fél kimenő címére
  sendConnection.setRemoteDescription(answerMessage.answer)
    // siker
    .then(() => {
      // ICE candidate-ek importálása
      answerMessage.icecandidates.forEach((c) => sendConnection.addIceCandidate(c));

      document.getElementById('status').innerHTML = 'Connected!';
    })
    .catch((error) => console.error(`Unable to establish P2P communication: ${error}`));
};

// gombnyomás az üzenetnél
const sendMessage = (keyPressEvent) => {
  // ENTER esetén
  if (keyPressEvent.keyCode === 13) {
    // üzenetet küldünk
    const message = document.getElementById('messageInput').value;
    sendChannel.send(message);
    // a szöveges bemenetet ürítjük
    document.getElementById('messageInput').value = '';
  }
};

window.onload = () => {
  connectPeers();
  document.getElementById('answerBtn').onclick = onAnswer;
  document.getElementById('messageInput').onkeypress = sendMessage;
};
