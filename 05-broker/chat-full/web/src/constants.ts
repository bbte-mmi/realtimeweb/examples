const w = window as unknown as Record<string, string>;

export const backendUrl = w.backendUrl || 'http://localhost:8080';

export const baseApiUrl = `${backendUrl}/backend`;
export const baseWsUrl = backendUrl;
