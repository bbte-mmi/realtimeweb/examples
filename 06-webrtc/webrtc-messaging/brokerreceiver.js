let receiveConnection; // fogadó fél kapcsolata
let receiveChannel;    // fogadó fél folyama

// STOMP kliens kapcsolati objektum deklarálása
let client;
let connectionId;
let subscription;

// bemenet tisztítása - XSS elkerülés
const sanitize = (string) => {
  const dummyElement = document.createElement('span');
  dummyElement.innerText = string;
  return dummyElement.innerHTML;
};

// új üzenet megjelenítése
const addMessage = (message) => {
  const elem = document.createElement('div');
  elem.innerHTML = `<b>Remote sender: </b>${sanitize(message.data)}`;
  document.getElementById('messages').appendChild(elem);
};

const onIcecandidate = (message) => {
  const icecandidate = JSON.parse(message.body);
  receiveConnection.addIceCandidate(icecandidate);
};

const onOffer = (message) => {
  // több üzenetet nem fogadunk innen
  subscription.unsubscribe();

  // kiolvassuk a másik fél ajánlatát a szöveges bemenetből
  const body = JSON.parse(message.body);

  connectionId = body.connectionId;

  // fogadó kapcsolat megnyitása
  receiveConnection = new RTCPeerConnection();
  // lekezeljük, hogy mi történik ha adatfolyam érkezik
  receiveConnection.ondatachannel = (channelEvent) => {
    document.getElementById('status').innerText = 'Connected!';
    receiveChannel = channelEvent.channel;
    receiveChannel.onmessage = addMessage;
  };

  // figyelünk a dedikált icecandidate-ekre
  client.subscribe(`/queue/webrtc-messaging-sender-icecandidate-${connectionId}`, onIcecandidate, { 'auto-delete': true });

  // közös ICE jelölteket biztosítunk
  receiveConnection.onicecandidate = (e) => {
    if (e.candidate) {
      const headers = { 'Content-Type': 'application/json', 'auto-delete': true };
      client.send(`/queue/webrtc-messaging-receiver-icecandidate-${connectionId}`, headers, JSON.stringify(e.candidate));
    }
  };

  // 1. a fogadó fél a beérkező címét beállítja a küldő fél kimenő címére
  receiveConnection.setRemoteDescription(body.offer)
    // 2. a fogadó fél választ generál (itt kap ő ICE nevet)
    .then(() => receiveConnection.createAnswer())
    // 3. a fogadó fél beállítja saját leírását
    .then((answer) => receiveConnection.setLocalDescription(answer))
    // 4. a fogadó fél publikálja a válaszát
    .then(() => {
      document.getElementById('status').innerText = 'Offer received, answer generated';
      const headers = { 'Content-Type': 'application/json', 'auto-delete': true };
      client.send(`/queue/webrtc-messaging-answer-${connectionId}`, headers, JSON.stringify(receiveConnection.localDescription));
    })
    .catch((error) => console.error(`Unable to establish P2P communication: ${error}`));
};

window.onload = () => {
  // Kapcsolat megnyitása
  // RabbitMQ STOMP-over-WebSocket kapcsolata
  // port = 15674, subprotocol = STOMP
  /* eslint-disable no-restricted-globals */
  /* eslint-disable no-undef */
  client = Stomp.client(`ws://${location.hostname}:15674/ws`);

  // sikeres kapcsolat esetén
  const onConnect = () => {
    // figyelünk az offer nevű queue-ra, de csak 1 üzenetet fogadunk
    subscription = client.subscribe('/queue/webrtc-messaging-offer', onOffer);
  };

  // STOMP kapcsolat
  client.connect('guest', 'guest', onConnect, console.error, '/');
};
