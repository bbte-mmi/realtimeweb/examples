# Példaprogramok

Az előadás különböző tematikáihoz tartozó példaprogramok elérhetőek egy [publikus git tárolón](https://gitlab.com/bbte-mmi/realtimeweb/examples) keresztül.

Előzetesen hasznos lehet végigböngészi a [Webprogramozás tárgy példaprogramjai](https://gitlab.com/bbte-mmi/webprog/examples)t is.

## Szerkezet

- A példaprogramok tematika szerint vannak csoportosítva. 
- Minden tematikának egy külön könyvtár felel meg, ezen belül megtalálhatóak az aktuális példaprogramok.

## Használat

### Első letöltés

- git *konzol*parancs:
    - navigáljunk azon mappába, ahova letöltenénk a példákat: `cd <celpont>`
    - letöltés: `git clone https://gitlab.com/bbte-mmi/realtimeweb/examples` (létrehoz egy `examples` almappát)
- *VS Code*-ból:
    - Command pallette: `Ctrl+Shift+P`
    - "Git: Clone"
    - Repo URL: `https://gitlab.com/bbte-mmi/realtimeweb/examples`

### A tartalom frissítése

- git *konzol*parancs:
    - navigáljunk azon mappába, ahova korábban letöltöttük a példákat: `cd <celpont>/examples`
    - frissítés: `git pull`
- *VS Code*-ból:  
![](vscode-sync.png){width="40%"}

### Hiba esetén

Hiba léphet fel, hogyha a lokális tárolóban levő tartalom *megváltozott* (ha kézzel változtattok a példaprogramok tartalmán). A könyvtár *tisztításáért* hívjátok a következőt ugyanazon `examples` mappából (**Vigyázat!:** Ezen parancsok meghívása a laborfeladatos tárolóban kitörölheti annak módosításait):  
```
    git reset --hard HEAD
    git clean -xfd
```

# *Bónusz:* SSH használata gittel

Annak érdekében, hogy ne kelljen minden pull/push operációkor *felhasználót és jelszót* megadj a gitnek, konfigurálható az SSH-s elérés. Ennek lépései:

1. Lokálisan a *Git Bash*-ből: `ssh-keygen` - minden kért argumentumot **üresen hagyva**.
2. `cat ~/.ssh/id_rsa.pub` - ez kiírja a publikus SSH kulcsot (figyeld az előző parancs kimenetét, azt a fájlt kell megnyitni, ami `id`-vel kezdődik és `.pub`-ra végződik, előfordulhat, hogy nem `rsa` lesz, hanem például `ed22519`).
3. GitLab-on a profilodon a *Preferences* -> *SSH Keys* részbe másold be az SSH kulcsot *teljességében* (`ssh-rsa` kulcsszó az elején, a géped neve a végén).
4. A projekt oldalán másold le az SSH-s elérési linket - ennek formája `git@gitlab.com:bbte-mmi/realtimeweb/...`
5. A fenti HTTP-s `clone` parancs helyett: `git clone <link>` Ha már klónoztad a repót korábban, akkor annak folderén belül: `git remote set-url origin <link>`
