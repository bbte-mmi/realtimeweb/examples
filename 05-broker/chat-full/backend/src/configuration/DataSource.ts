import { DataSource } from 'typeorm';
import models from '../model';

export function initializeDataSource(): Promise<DataSource> {
  return new DataSource({
    type: 'mysql',
    host: process.env.MYSQL_HOST || 'localhost',
    port: Number(process.env.MYSQL_PORT) || 3306,
    username: process.env.MYSQL_USER || 'chat',
    password: process.env.MYSQL_PASSWORD || 'chat',
    database: process.env.MYSQL_DATABASE || 'chat',
    dropSchema: Boolean(process.env.DROP_SQL),
    logging: Boolean(process.env.SHOW_SQL),
    entities: models,
    synchronize: true,
  }).initialize();
}

const DataSourceProvider = {
  provide: DataSource,
  useFactory: () => initializeDataSource(),
};
export default DataSourceProvider;
