import react from '@vitejs/plugin-react';
import { defineConfig } from 'vite';

const reactPlugin = react();

const config = defineConfig({
  plugins: [reactPlugin],
});

export default config;
