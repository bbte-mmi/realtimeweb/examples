export type ChannelDto = {
  id: number;
  name: string;
};

export type ChannelCreationDto = {
  name: string;
};
