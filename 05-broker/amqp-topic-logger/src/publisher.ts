/**
 * Ez a szkript egyetlen log üzenetet küld az alapértelmezett topic-ra
 * Parancssori argumentumokból nyerjük ki a szintet és az üzenetet.
 *
 * Meghívás: ts-node src/publisher.ts -l error -t "A hibaüzenetem"
 */
import amqp from 'amqplib';
import crypto from 'crypto';
import yargs from 'yargs';

(async () => {
  const argv = await yargs
    .option('logLevel', {
      alias: 'l',
      describe: 'Log level of your message',
      choices: ['debug', 'info', 'warn', 'error'],
      default: 'info',
    })
    .option('text', {
      alias: 't',
      describe: 'Message you want to send',
      demandOption: true,
    })
    .help()
    .parse();

  const message = JSON.stringify({
    clientId: crypto.randomBytes(8).toString('base64'),
    logLevel: argv.logLevel,
    time: new Date(),
    text: argv.text,
  });
  const routingKey = `logger.${argv.logLevel}`;

  console.log(`Sending following message to routing key ${routingKey}:`, message);

  const connection = await amqp.connect({});
  const channel = await connection.createChannel();

  channel.publish('amq.topic', routingKey, Buffer.from(message));

  await channel.close();
  await connection.close();
})();
