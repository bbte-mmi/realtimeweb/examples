import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { createBlogPost, findAllUsers } from '../api';
import ErrorMessage from './ErrorMessage';

import { BlogPostCreateDto } from '../dto';
import './BlogPostEdit.css';

export default function BlogPostEdit() {
  const [blogPost, setBlogPost] = useState<BlogPostCreateDto>({
    title: '',
    authorId: '',
    content: '',
  });
  const [users, setUsers] = useState([]);
  const [error, setError] = useState('');

  useEffect(() => {
    (async () => {
      const newUsers = await findAllUsers();
      setUsers(newUsers);

      if (!blogPost.authorId && newUsers.length > 0) {
        setBlogPost({
          ...blogPost,
          authorId: newUsers[0].id,
        });
      }
    })();
  }, []);

  const navigate = useNavigate();

  const onSubmit = async (event) => {
    event.preventDefault();
    try {
      await createBlogPost(blogPost);
      navigate('/');
    } catch (e) {
      setError(e.message);
    }
  };

  const onChange = (event) => {
    setBlogPost({
      ...blogPost,
      [event.target.name]: event.target.value,
    });
  };

  return (
    <div className="form-container">
      <ErrorMessage message={error} />
      <form onSubmit={onSubmit}>
        <div className="row">
          <div className="col">
            <div className="form-group row">
              <div className="col-md-6">
                <label htmlFor="title">
                  Title&nbsp;
                  <input
                    type="text"
                    className="form-control"
                    name="title"
                    value={blogPost.title}
                    onChange={onChange}
                    placeholder="Enter title"
                  />
                </label>
              </div>
              <div className="col-md-6">
                <label htmlFor="author">
                  Author ID&nbsp;
                  <select className="form-control" name="authorId" id="authorId" onChange={onChange}>
                    {users.map((user) => (
                      <option key={user.id} value={user.id}>
                        {user.fullName}
                      </option>
                    ))}
                  </select>
                </label>
              </div>
            </div>
            <div className="form-group row">
              <div className="col-md-12">
                <label htmlFor="description">
                  Content&nbsp;
                  <textarea
                    className="form-control"
                    name="content"
                    value={blogPost.content}
                    onChange={onChange}
                    placeholder="Enter text..."
                  />
                </label>
              </div>
            </div>
            <div className="form-group row">
              <div className="col-md-6">
                <input type="submit" className="btn btn-outline-primary" value="Submit" />
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  );
}
