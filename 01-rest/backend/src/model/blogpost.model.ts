import { ObjectId } from 'mongodb';
import { BaseEntity } from './base';

export interface BlogPost extends BaseEntity {
  title: string;
  content: string;
  authorId: ObjectId;
  date: Date;
}
