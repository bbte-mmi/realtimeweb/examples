import { BaseEntity } from './base';

export interface User extends BaseEntity {
  fullName: string;
}
