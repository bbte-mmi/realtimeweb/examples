import { useEffect, useState } from 'react';
import { createRoot } from 'react-dom/client';

import './style.css';

//
// esemény DTO
//

type Message = {
  text: string;
  time: string;
};

function MessagesFromServer() {
  const [messages, setMessages] = useState<Array<Message>>([]);

  // első mountkor kapcsolat az SSE-vel
  useEffect(() => {
    // Kapcsolat megnyitása a dedikált SSE endpoint fele
    const eventSource = new EventSource('http://localhost:8080/messages');

    // ha üzenetet kapunk a szervertől, konkatenáljuk az állapot-listánkhoz
    eventSource.onmessage = (event: MessageEvent<string>) => {
      const message: Message = JSON.parse(event.data);
      setMessages((m) => [...m, message]);
    };

    // ha a komponenst zárjuk, zárjuk az SSE kapcsolatot is
    return () => eventSource.close();
  }, []);

  return (
    <>
      <h1>Messages received through SSE</h1>

      <div id="messages">
        {messages.map((message) => (
          <div key={`${message.time}`} className="them">
            <span className="name">Server&nbsp;</span>
            <span>({new Date(message.time).toLocaleString()}):&nbsp;</span>
            <span className="msg">{message.text}</span>
          </div>
        ))}
      </div>
    </>
  );
}

const root = document.getElementById('root') as HTMLDivElement;
createRoot(root).render(<MessagesFromServer />);
