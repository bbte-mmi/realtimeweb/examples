import { FormEvent, useCallback, useEffect, useMemo, useState } from 'react';
import { createRoot } from 'react-dom/client';

import './style.css';

//
// esemény DTO
//

type BroadcastMessage = {
  clientId: string;
  text: string;
  time: string;
};

function ChatWebSocket({ wsUrl }: { wsUrl: string }) {
  const [text, setText] = useState<string>('');
  const [messages, setMessages] = useState<Array<BroadcastMessage>>([]);

  // random ID
  const clientId = useMemo(() => {
    return btoa(crypto.getRandomValues(new Uint8Array(2)).toString());
  }, []);

  // Kapcsolat megnyitása a dedikált WS felé
  const ws = useMemo(() => new WebSocket(`${wsUrl}?clientId=${clientId}`), [clientId, wsUrl]);

  useEffect(() => {
    // ha üzenetet kapunk a szervertől
    ws.onmessage = (event: MessageEvent<string>) => {
      const message: BroadcastMessage = JSON.parse(event.data);
      setMessages((m) => [...m, message]);
    };

    return () => {
      console.log('Disconnecting from WebSocket');
      ws.close();
    };
  }, [clientId]);

  // gombnyomás az üzenetnél
  const sendMessage = useCallback(
    (event: FormEvent) => {
      // szinkron leadás elkerülése
      event.preventDefault();

      // üzenetet küldünk
      const message: BroadcastMessage = {
        clientId,
        text,
        time: new Date().toISOString(),
      };

      // WebSockettel küldjük az üzenetet
      ws.send(JSON.stringify(message));

      // a szöveges bemenetet ürítjük
      setText('');
    },
    [clientId, text, messages],
  );

  return (
    <>
      <div>
        <b>Status:</b> Connected as <code>{clientId}</code>
      </div>

      <div id="messages">
        {messages.map((message) => (
          <div key={`${message.clientId}-${message.time}`} className={message.clientId === clientId ? 'me' : 'them'}>
            <span className="name">{message.clientId}:&nbsp;</span>
            <span className="msg">{message.text}</span>
          </div>
        ))}
      </div>

      {/* <!-- form, amin keresztül üzenetet küldhetünk --> */}
      <form onSubmit={sendMessage}>
        <input
          style={{ textAlign: 'right' }}
          type="text"
          value={text}
          onChange={(event) => setText(event.target.value)}
          placeholder="Type message and press ENTER..."
        />
      </form>
    </>
  );
}

export function WsUrlSetter() {
  const [wsUrl, setWsUrl] = useState<string>('ws://localhost:8080/chat');
  const [connected, setConnected] = useState<boolean>(false);

  const onConnect = useCallback((event: FormEvent) => {
    // szinkron leadás elkerülése
    event.preventDefault();
    setConnected(true);
  }, []);

  return (
    <>
      <h1>Scalable chat with WebSocket + redis</h1>

      <form onSubmit={onConnect}>
        <input type="text" value={wsUrl} onChange={(event) => setWsUrl(event.target.value)} disabled={connected} />
      </form>

      {connected && <ChatWebSocket wsUrl={wsUrl} />}
    </>
  );
}

const root = document.getElementById('root') as HTMLDivElement;
createRoot(root).render(<WsUrlSetter />);
