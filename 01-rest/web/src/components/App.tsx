import { Navigate, Route, Routes, useNavigate } from 'react-router-dom';
import BlogPostDetails from './BlogPostDetails';
import BlogPostEdit from './BlogPostEdit';
import BlogPosts from './BlogPosts';
import ErrorMessage from './ErrorMessage';
import Menu from './Menu';
import Users from './Users';

type AppProps = {
  error?: string;
};

export default function App({ error = null }: AppProps) {
  const navigate = useNavigate();

  const onSearch = (term: string) => {
    navigate(`/blogPosts?term=${encodeURIComponent(term)}`);
  };

  return (
    <div>
      <Menu onSearch={onSearch} />
      <ErrorMessage message={error} />

      <Routes>
        <Route path="/blogPosts" element={<BlogPosts />} />
        <Route path="/blogPosts/:blogPostId" element={<BlogPostDetails />} />
        <Route path="/users" element={<Users />} />
        <Route path="/createBlogPost" element={<BlogPostEdit />} />
        <Route path="*" element={<Navigate to="/blogPosts" replace />} />
      </Routes>
    </div>
  );
}
