import * as amqp from 'amqplib';

/**
 * Egyszerű üzenetekre való figyelés AMQP protokollal
 * a lokálisan futó RabbitMQ "hello" nevű queue-jából.
 * Ebben a példában ez a szkript játsza a "consumer" szerepet.
 */
(async () => {
  const connection = await amqp.connect({});
  const channel = await connection.createChannel();

  // exchange készítése
  await channel.assertExchange('example-direct-exchange', 'direct');
  // sor készítése
  await channel.assertQueue('example-queue');
  // az exchange és sor összekötése
  await channel.bindQueue('example-queue', 'example-direct-exchange', 'example-queue');

  // figyelünk az "example-queue" nevű queue-ra küldött üzenetekre
  // minden beérkezett üzenet a callback függvényt triggereli
  channel.consume('example-queue', (message) => {
    if (message) {
      // loggoljuk az üzenetet
      console.log('Received following message:', message?.content?.toString());
      // jóváhagyjuk az üzenetet
      channel.ack(message);
    }
  });

  // SIGINT signal (Ctrl+C) esetén zárjuk a kapcsolatot
  process.once('SIGINT', async () => {
    console.log('Closing AMQP connection');
    await channel.close();
    await connection.close();
  });

  console.log('Listening for messages');
})();
