import { Client, IMessage } from '@stomp/stompjs';
import { FormEvent, useCallback, useEffect, useMemo, useState } from 'react';
import { createRoot } from 'react-dom/client';

import './style.css';

//
// socket io DTOs
//

type BroadcastMessage = {
  clientId: string;
  text: string;
  time: string;
};

function ChatStomp() {
  const [connected, setConnected] = useState<boolean>(false);
  const [text, setText] = useState<string>('');
  const [messages, setMessages] = useState<Array<BroadcastMessage>>([]);

  // random ID
  const clientId = useMemo(() => {
    return btoa(crypto.getRandomValues(new Uint8Array(2)).toString());
  }, []);

  // STOMP kliens
  const client = useMemo(() => {
    const c = new Client({
      brokerURL: 'ws://localhost:15674/ws',
    });

    // sikeres kapcsolat esetén
    c.onConnect = () => {
      console.log('Connected to STOMP');
      setConnected(true);
      // feliratkozunk a válaszok queue-jára
      // saját válasz queue-nk lesz, hogy több master csatlakozhasson
      // callback visszajön minden üzenetre
      // mivel dinamikus queue, headerekkel állítjuk be, hogy törlődjön bróker restart után
      const headers = { exclusive: 'true', 'auto-delete': 'true' };
      const onMessage = (message: IMessage) => {
        const response = JSON.parse(message.body) as BroadcastMessage;
        console.log('Receiving', response);
        setMessages((m) => [...m, response]);
      };
      c.subscribe('/exchange/chat', onMessage, headers);
    };

    return c;
  }, [clientId]);

  useEffect(() => {
    console.log('Connecting to STOMP...');
    client.activate();

    return () => {
      console.log('Disconnecting from STOMP...');
      client.deactivate();
    };
  }, [client]);

  // gombnyomás az üzenetnél
  const sendMessage = useCallback(
    (event: FormEvent) => {
      // szinkron leadás elkerülése
      event.preventDefault();

      // üzenetet küldünk
      const message: BroadcastMessage = {
        clientId,
        text,
        time: new Date().toISOString(),
      };

      // STOMP-on küldjük az üzenetet
      client.publish({
        destination: '/exchange/chat',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(message),
      });

      // a szöveges bemenetet ürítjük
      setText('');
    },
    [clientId, text],
  );

  return (
    <>
      <h1>Chat with STOMP + WebSocket</h1>

      <div id="status">
        <b>Status:</b>&nbsp;
        {connected ? (
          <>
            Connected as <code>{clientId}</code>
          </>
        ) : (
          <>not connected...</>
        )}
      </div>

      <div id="messages">
        {messages.map((message) => (
          <div key={`${message.clientId}-${message.time}`} className={message.clientId === clientId ? 'me' : 'them'}>
            <span className="name">{message.clientId}:&nbsp;</span>
            <span className="msg">{message.text}</span>
          </div>
        ))}
      </div>

      {/* <!-- form, amin keresztül üzenetet küldhetünk --> */}
      <form onSubmit={sendMessage}>
        <input
          style={{ textAlign: 'right' }}
          type="text"
          value={text}
          onChange={(event) => setText(event.target.value)}
          placeholder="Type message and press ENTER..."
        />
      </form>
    </>
  );
}

const root = document.getElementById('root') as HTMLDivElement;
createRoot(root).render(<ChatStomp />);
