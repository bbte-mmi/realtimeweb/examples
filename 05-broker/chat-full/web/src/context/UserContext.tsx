import React from 'react';
import { UserDto } from '../dto/UserDto';

const UserContext = React.createContext<UserDto>(null);
export default UserContext;
