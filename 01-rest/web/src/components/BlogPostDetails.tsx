import { useEffect, useState } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';
import { deleteBlogPost, findBlogPostById, findUserById } from '../api';
import { BlogPostDetailsDto, UserDto } from '../dto';

import './BlogPostDetails.css';

export default function BlogPostDetails() {
  const [blogPost, setBlogPost] = useState<BlogPostDetailsDto>(null);
  const [author, setAuthor] = useState<UserDto | undefined>(null);

  const { blogPostId } = useParams();
  const navigate = useNavigate();

  useEffect(() => {
    (async () => {
      const newBlogPost = await findBlogPostById(blogPostId);
      setBlogPost(newBlogPost);
      const newAuthor = await findUserById(newBlogPost.authorId);
      setAuthor(newAuthor);
    })();
  }, []);

  if (blogPost === null || author === null) {
    return <div>Loading...</div>;
  }

  const onDelete = async () => {
    await deleteBlogPost(blogPostId);
    navigate('/');
  };

  return (
    <div className="jumbotron">
      <Link to="/">
        <button type="button" className="btn btn-outline-info back-btn">
          &lt; Back
        </button>
      </Link>
      <div className="row">
        <div className="col col-md-12">
          <h3>{blogPost.title}</h3>
          <div>
            <i>by </i>
            {author ? (
              <>
                {author.fullName} (ID {author.id})
              </>
            ) : (
              'Unknown'
            )}
          </div>
          <div>
            <input type="button" className="btn btn-danger btn-sm" onClick={onDelete} value="Delete" />
          </div>
          <div className="row">
            <div className="col">
              <hr className="my-4" />
            </div>
          </div>
          <div>{blogPost.content}</div>
        </div>
      </div>
    </div>
  );
}
