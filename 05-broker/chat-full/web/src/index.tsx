import React, { useEffect, useState } from 'react';
import { io, Socket } from 'socket.io-client';
import { createRoot } from 'react-dom/client';

import UserContext from './context/UserContext';
import SimpleInput from './components/SimpleInput';
import { fetchOrCreateUser } from './api/UserService';
import Main from './components/Main';
import { UserDto } from './dto/UserDto';
import SocketContext from './context/SocketContext';
import { baseWsUrl } from './constants';

import './style.css';

function App() {
  const [user, setUser] = useState<UserDto>(null);
  const [socket, setSocket] = useState<Socket>(null);

  const login = async (name: string) => {
    const newUser = await fetchOrCreateUser(name);
    setUser(newUser);
  };

  useEffect(() => {
    const newSocket = io(baseWsUrl);
    setSocket(newSocket);
  }, []);

  return (
    <UserContext.Provider value={user}>
      <SocketContext.Provider value={socket}>
        <div className="container">
          <h1>Chat w/ REST+socket.io+RabbitMQ</h1>
          {user ? (
            <Main logout={() => setUser(null)} />
          ) : (
            <SimpleInput placeholder="Type name and press ENTER..." onEnter={login} />
          )}
        </div>
      </SocketContext.Provider>
    </UserContext.Provider>
  );
}

const container = document.getElementById('root');
const root = createRoot(container);
root.render(<App />);
