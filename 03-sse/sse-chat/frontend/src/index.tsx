import { FormEvent, useCallback, useEffect, useMemo, useState } from 'react';
import { createRoot } from 'react-dom/client';

import './style.css';

//
// esemény DTO
//

type BroadcastMessage = {
  clientId: string;
  text: string;
  time: string;
};

function ChatSse() {
  // random ID
  const clientId = useMemo(() => {
    return btoa(crypto.getRandomValues(new Uint8Array(2)).toString());
  }, []);

  const [text, setText] = useState<string>('');
  const [messages, setMessages] = useState<Array<BroadcastMessage>>([]);

  // gombnyomás az üzenetnél
  const sendMessage = useCallback(
    async (event: FormEvent) => {
      // szinkron leadás elkerülése
      event.preventDefault();

      // üzenetet küldünk
      const message: BroadcastMessage = {
        clientId,
        text,
        time: new Date().toISOString(),
      };

      // aszinkron requesttel küldjük az üzenetet
      await fetch('http://localhost:8080/chat', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(message),
      });

      // a szöveges bemenetet ürítjük
      setText('');
    },
    [clientId, text, messages],
  );

  useEffect(() => {
    // Kapcsolat megnyitása a dedikált SSE endpoint fele
    const eventSource = new EventSource(`http://localhost:8080/chat?clientId=${clientId}`);

    // ha üzenetet kapunk a szervertől
    eventSource.onmessage = (event: MessageEvent<string>) => {
      const message: BroadcastMessage = JSON.parse(event.data);
      setMessages((m) => [...m, message]);
    };

    return () => {
      console.log('Disconnecting from SSE');
      eventSource.close();
    };
  }, [clientId]);

  return (
    <>
      <h1>Chat with SSE</h1>

      <div>
        <b>Status:</b> Connected as <code>{clientId}</code>
      </div>

      <div id="messages">
        {messages.map((message) => (
          <div key={`${message.clientId}-${message.time}`} className={message.clientId === clientId ? 'me' : 'them'}>
            <span className="name">{message.clientId}:&nbsp;</span>
            <span className="msg">{message.text}</span>
          </div>
        ))}
      </div>

      {/* <!-- form, amin keresztül üzenetet küldhetünk --> */}
      <form onSubmit={sendMessage}>
        <input
          type="text"
          value={text}
          onChange={(event) => setText(event.target.value)}
          placeholder="Type message and press ENTER..."
        />
      </form>
    </>
  );
}

const root = document.getElementById('root') as HTMLDivElement;
createRoot(root).render(<ChatSse />);
