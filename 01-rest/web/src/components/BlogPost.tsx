import { useState } from 'react';
import { Link } from 'react-router-dom';
import { deleteBlogPost } from '../api/blogpost.api';
import { BlogPostDto } from '../dto/blogpost.dto';

type BlogPostProps = {
  blogPost: BlogPostDto;
};

export default function BlogPost(props: BlogPostProps) {
  const [deleted, setDeleted] = useState(false);

  if (deleted) {
    return <div>Blog post deleted</div>;
  }

  const { blogPost } = props;

  const onDelete = async () => {
    await deleteBlogPost(blogPost.id);
    setDeleted(true);
  };

  return (
    <>
      <div className="row">
        <div className="col col-md-12">
          <Link to={`/blogPosts/${blogPost.id}`}>
            <h3>{blogPost.title}</h3>
          </Link>
          <div>
            <input type="button" className="btn btn-danger btn-sm" onClick={onDelete} value="Delete" />
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col">
          <hr className="my-4" />
        </div>
      </div>
    </>
  );
}
