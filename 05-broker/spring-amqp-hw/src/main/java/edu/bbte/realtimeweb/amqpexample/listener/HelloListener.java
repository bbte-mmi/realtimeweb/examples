package edu.bbte.realtimeweb.amqpexample.listener;

import edu.bbte.realtimeweb.amqpexample.dto.HelloMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

/**
 * Spring bean, amelyik figyel egy queue-ra küldött üzenetekre
 */
@Component
@Profile("listener")
@Slf4j
public class HelloListener {

    /**
     * Ha nem létezik a "hello" queue, megadhatjuk a "queuesToDeclare" kulcsban is
     */
    @RabbitListener(queues = "hello")
    public void onMessage(HelloMessage message) {
        log.info("Received on 'hello' queue: {}", message);
    }
}
