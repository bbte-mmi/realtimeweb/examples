package edu.bbte.realtimeweb.amqpexample.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.jackson.Jacksonized;

/**
 * DTO, amelyet küldünk/fogadunk a "hello" AMQP soron
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Jacksonized
public class HelloMessage {
    String name;
    Integer age;
}
