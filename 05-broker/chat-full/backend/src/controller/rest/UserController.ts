import { Body, Controller, Get, Inject, Query, Post } from '@nestjs/common';
import { UserCreationDto, UserDto } from '../../dto/UserDto';
import * as userMapper from '../../mapper/UserMapper';
import User from '../../model/User';
import UserService from '../../service/UserService';

@Controller('/backend/users')
export default class UserController {
  @Inject()
  private userService: UserService;

  @Get()
  async findAllUsers(@Query('name') name: string): Promise<UserDto[]> {
    let users: User[];
    if (name) {
      users = await this.userService.findUsersByName(name);
    } else {
      users = await this.userService.findAllUsers();
    }

    return userMapper.modelsToDtos(users);
  }

  @Post()
  async createUser(@Body() dto: UserCreationDto): Promise<UserDto> {
    const model = userMapper.dtoToModel(dto);
    const user = await this.userService.createUser(model);
    return userMapper.modelToDto(user);
  }
}
