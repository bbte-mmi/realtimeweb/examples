import 'reflect-metadata';
import bodyParser from 'body-parser';
import cors from 'cors';
import morgan from 'morgan';
import { NestFactory } from '@nestjs/core';
import { Module } from '@nestjs/common';
import { NestExpressApplication } from '@nestjs/platform-express';

import providers from './configuration';
import services from './service';
import restControllers from './controller/rest';
import wsControllers from './controller/ws';

@Module({
  imports: [],
  controllers: [...restControllers],
  providers: [...providers, ...services, ...wsControllers],
})
export class AppModule {}

// aszinkron wrap a fő állományba
(async () => {
  // NestJS Express alkalmazás felépítése
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  app.use(morgan('tiny'), cors(), bodyParser.json());

  const port = process.env.PORT || 8080;
  await app.listen(port);
  console.log(`Server listening on http://localhost:${port}/ ...`);
})();
