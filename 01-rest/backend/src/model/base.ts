import { ObjectId } from 'mongodb';

export interface BaseEntity {
  _id: ObjectId;
}

export type Unpersisted<E extends BaseEntity> = Omit<E, '_id'>;
