import * as amqp from 'amqplib';
import { randomBytes } from 'crypto';
import { createInterface } from 'readline';

type Message = {
  clientId: string;
  text: string;
  time: string;
};

const exchange = 'chat';
const clientId = randomBytes(8).toString('base64');
const queueName = `chat-${clientId}`;

(async () => {
  const connection = await amqp.connect({});
  const channel = await connection.createChannel();

  // biztosan kell létezzen a fanout típusú exchange
  await channel.assertExchange(exchange, 'fanout');

  // üzenetbeolvasási konzol, amin kereszül küldünk üzeneteket
  const rl = createInterface(process.stdin, process.stdout);
  console.log(`Your ID is ${clientId}. Enter text to send to chat`);
  rl.setPrompt(`${clientId} > `);
  rl.on('line', (text: string) => {
    // elküldjük a fanout exchange-re a sort
    const message: Message = { clientId, text, time: new Date().toISOString() };
    // a routing key üres, mivel a fanout nem veszi figyelembe
    channel.publish(exchange, '', Buffer.from(JSON.stringify(message)));
    rl.prompt();
  });
  rl.prompt();

  // dedikált queue-t kell készítsünk a kliensünknek, mely csak neki szól,
  await channel.assertQueue(queueName, { exclusive: true });
  // s binding van rá a "chat" fanout exchange-ről
  // a routing pattern lényegtelen, a fanout nem veszi figyelembe
  await channel.bindQueue(queueName, exchange, '');

  // a dedikált queue-ra figyelünk
  channel.consume(queueName, (data) => {
    if (data) {
      const message = JSON.parse(data.content.toString()) as Message;
      if (message.clientId !== clientId) {
        console.log(`\n${message.clientId} (${message.time}): ${message.text}`);
        rl.prompt();
      }
      channel.ack(data);
    }
  });

  process.once('SIGINT', async () => {
    // zárjuk a csatornát és kapcsolatot
    rl.close();
    console.log('\nClosing AMQP connection');
    await channel.close();
    await connection.close();
  });
})();
