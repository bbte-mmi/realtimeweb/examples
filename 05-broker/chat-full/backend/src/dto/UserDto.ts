export type UserDto = {
  id: number;
  name: string;
};

export type UserCreationDto = {
  name: string;
};
