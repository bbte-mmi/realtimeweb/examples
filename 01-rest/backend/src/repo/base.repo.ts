import { Collection, Filter, ObjectId, OptionalUnlessRequiredId } from 'mongodb';
import { BaseEntity, Unpersisted } from '../model/base';

/**
 * Generikus adattal dolgozó Mongo adatelérési objektum
 * Alap CRUD műveleteket tartalmazza
 */
export class Repository<T extends BaseEntity> {
  collection: Collection<T>;

  constructor(collection: Collection<T>) {
    this.collection = collection;
  }

  // összes sor visszatérítése
  findAll(): Promise<T[]> {
    return this.collection.find<T>({}, {}).toArray();
  }

  // ID alapján keresés
  findById(_id: ObjectId): Promise<T | null> {
    const filter = { _id } as Filter<T>;
    return this.collection.findOne<T>(filter);
  }

  // ID alapján verifikáció hogy az entitás létezik-e
  // Ezalapján hozzáköthetünk új blog postokat anélkül hogy betöltsük memóriába.
  existsById(_id: ObjectId): Promise<boolean> {
    const filter = { _id } as Filter<T>;
    return this.collection.find<T>(filter, { limit: 1 }).hasNext();
  }

  // Beszúrás generált ID-val
  // Visszatérítjük az entitás állapotát generált ID-val
  async create(entity: Unpersisted<T>): Promise<T> {
    const result = await this.collection.insertOne(entity as OptionalUnlessRequiredId<T>);
    return {
      ...entity,
      _id: result.insertedId,
    } as unknown as T;
  }

  // Módosítás vagy beszúrás
  // Visszatérítjük az entitás teljes állapotát
  async update(entity: T): Promise<T> {
    const filter = { _id: entity._id } as Filter<T>;
    await this.collection.updateOne(filter, { $set: entity });
    return entity;
  }

  // Törlés
  // Továbbítjuk hogy létezett-e az entitás (érintettünk-e több mint 0 sort)
  async delete(_id: ObjectId): Promise<boolean> {
    const filter = { _id } as Filter<T>;
    const result = await this.collection.deleteOne(filter);
    return result.deletedCount > 0;
  }

  // Törlés
  // Továbbítjuk hogy létezett-e az entitás (érintettünk-e több mint 0 sort)
  async deleteAll(): Promise<boolean> {
    const result = await this.collection.deleteMany({});
    return result.deletedCount > 0;
  }
}
