package edu.bbte.realtimeweb.amqpexample.sender;

import edu.bbte.realtimeweb.amqpexample.dto.HelloMessage;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@Profile("sender")
@Slf4j
@RequiredArgsConstructor
public class HelloSender {
    private final RabbitTemplate rabbitTemplate;

    /**
     * 15 másodpercenként lefutó küldés
     */
    @Scheduled(cron = "*/15 * * * * *")
    public void sendMessage() {
        var message = HelloMessage.builder().name("Some name").age(42).build();
        log.info("Sending to 'hello' queue: {}", message);
        rabbitTemplate.convertAndSend("hello", message);
    }
}
