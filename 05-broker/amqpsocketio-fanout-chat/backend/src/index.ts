/**
 * Chat alkalmazás ws segítségével.
 */
import http from 'http';
import { Server } from 'socket.io';
import { BroadcastMessage, RabbitMqChatClient, connectToRabbitMq } from './amqpclient';

interface ServerToClientEvents {
  message: (message: BroadcastMessage) => void;
  assignid: (id: string) => void;
}

interface ClientToServerEvents {
  message: (messageText: string) => void;
}

(async () => {
  const server = http.createServer();

  // csatlakozás a RabbitMQ-ra
  await connectToRabbitMq();

  // Socket.IO szerver
  const io = new Server<ClientToServerEvents, ServerToClientEvents>(server, {
    cors: { origin: '*' },
  });

  // websocket csatlakozásának lekezelése
  io.on('connection', async (socket) => {
    // egy broker kapcsolat a kliensnek
    const client = new RabbitMqChatClient();
    await client.createClientQueue();

    console.log(`[${client.clientId}] Socket connected via AMQP: ${socket.conn.remoteAddress}`);

    /**
     * Üzenetküldéskor továbbítjuk a brokernek
     */
    socket.on('message', (messageText) => {
      console.log(`[${client.clientId}] sent '${messageText}', relaying`);
      client.sendMessage(messageText);
    });

    /**
     * Brókertől jövő üzenetküldéskor továbbítjuk mindenkinek,
     * aki a mi szerverpéldányunkra van kapcsolódva
     */
    client.onMessage((message) => {
      socket.emit('message', message);
    });

    // egyedi azonosító eseményben való küldése
    socket.emit('assignid', client.clientId);
  });

  const port = process.env.PORT || 8080;
  server.listen(port, () => {
    console.log(`Server listening on http://localhost:${port}/ ...`);
  });
})();
