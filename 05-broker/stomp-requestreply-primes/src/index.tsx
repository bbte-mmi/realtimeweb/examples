import { Client, IMessage } from '@stomp/stompjs';
import { FormEvent, useCallback, useEffect, useMemo, useState } from 'react';
import { createRoot } from 'react-dom/client';

import './style.css';

//
// interakció DTO
//

type PrimeRequest = {
  number: number;
  replyQueue: string;
};

type PrimeReply = {
  number: number;
  result: boolean;
};

function StompPrimeCalculator() {
  const [connected, setConnected] = useState<boolean>(false);
  const [number, setNumber] = useState<number>(0);
  const [replies, setReplies] = useState<Array<PrimeReply>>([]);

  // random ID
  const clientId = useMemo(() => {
    return btoa(crypto.getRandomValues(new Uint8Array(2)).toString());
  }, []);

  // STOMP kliens
  const client = useMemo(() => {
    const c = new Client({
      brokerURL: 'ws://localhost:15674/ws',
    });

    // sikeres kapcsolat esetén
    c.onConnect = () => {
      console.log('Connected to STOMP');
      setConnected(true);
      // feliratkozunk a válaszok queue-jára
      // saját válasz queue-nk lesz, hogy több master csatlakozhasson
      // callback visszajön minden üzenetre
      // mivel dinamikus queue, headerekkel állítjuk be, hogy törlődjön bróker restart után
      const headers = { exclusive: 'true', 'auto-delete': 'true' };
      const onMessage = (message: IMessage) => {
        const response = JSON.parse(message.body) as PrimeReply;
        console.log('Receiving', response);
        setReplies((r) => [...r, response]);
      };
      c.subscribe(`/queue/isprimeresult-${clientId}`, onMessage, headers);
    };

    return c;
  }, [clientId]);

  useEffect(() => {
    console.log('Connecting to STOMP...');
    client.activate();

    return () => {
      console.log('Disconnecting from STOMP...');
      client.deactivate();
    };
  }, [client]);

  // gombnyomás az üzenetnél
  const sendMessage = useCallback(
    (event: FormEvent) => {
      // szinkron leadás elkerülése
      event.preventDefault();

      // üzenetet küldünk
      const message: PrimeRequest = {
        number,
        replyQueue: `isprimeresult-${clientId}`,
      };

      console.log('Sending', message);

      // STOMP-on küldjük az üzenetet
      client.publish({
        destination: '/queue/isprime',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(message),
      });
    },
    [client, number],
  );

  return (
    <>
      <h1>Prime number calculator</h1>

      <div id="status">
        <b>Status:</b>&nbsp;
        {connected ? (
          <>
            Connected as <code>{clientId}</code>
          </>
        ) : (
          <>not connected...</>
        )}
      </div>

      {/* text field ahol bekérjük a számot */}
      <form onSubmit={sendMessage}>
        <input
          id="numberInput"
          type="number"
          placeholder="Type number and press ENTER..."
          value={number}
          onChange={(event) => setNumber(Number(event.target.value))}
        />
      </form>

      <table id="results">
        <thead>
          <tr>
            <th>Number</th>
            <th>Result</th>
          </tr>
        </thead>
        <tbody>
          {replies.map((reply) => (
            <tr key={`${reply.number}-${reply.result}`}>
              <td>{reply.number}</td>
              <td>{`${reply.result}`}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  );
}

const root = document.getElementById('root') as HTMLDivElement;
createRoot(root).render(<StompPrimeCalculator />);
