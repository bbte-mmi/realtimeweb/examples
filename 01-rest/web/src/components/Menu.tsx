import { useState } from 'react';
import { Link } from 'react-router-dom';

type MenuProps = {
  onSearch: (term: string) => unknown;
};

export default function Menu({ onSearch }: MenuProps) {
  const [term, setTerm] = useState<string>('');

  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
      <Link className="navbar-brand" to="/">
        Blog
      </Link>

      <ul className="navbar-nav mr-auto">
        <li className="nav-item">
          <Link className="nav-link" to="/blogPosts">
            Posts
          </Link>
        </li>
        <li className="nav-item">
          <Link className="nav-link" to="/createBlogPost">
            Create post
          </Link>
        </li>
        <li className="nav-item">
          <Link className="nav-link" to="/users">
            Users
          </Link>
        </li>
      </ul>
      <input
        className="mr-sm-2"
        type="text"
        placeholder="Search"
        value={term}
        onChange={(e) => setTerm(e.target.value)}
      />
      <button
        className="btn btn-secondary btn-sm my-2 my-sm-0"
        type="button"
        onClick={() => {
          onSearch(term);
          setTerm('');
        }}
      >
        Search
      </button>
    </nav>
  );
}
