import { useEffect, useMemo, useRef, useState } from 'react';
import { createRoot } from 'react-dom/client';
import { Socket, io } from 'socket.io-client';

import './style.css';

//
// socket io DTOs
//

interface ServerToClientEvents {
  addFilename: (filename: string) => void;
}

interface ClientToServerEvents {
  chunk: (chunk: Blob) => void;
}

function MediaRecorderUpload() {
  const [filenames, setFilenames] = useState<string[]>([]);
  const [recording, setRecording] = useState<boolean>(false);
  const localVideoRef = useRef<HTMLVideoElement>(null);
  const [recorder, setRecorder] = useState<MediaRecorder>();

  // Kapcsolat megnyitása a dedikált WS felé
  const socket = useMemo<Socket<ServerToClientEvents, ClientToServerEvents>>(() => io('http://localhost:8080/'), []);

  useEffect(() => {
    // ha üzenetet kapunk a szervertől
    socket.on('addFilename', (fn: string) => {
      setFilenames((fns) => [...fns, fn]);
    });

    // unmountkor zárjuk a socket.io kapcsolatot
    return () => {
      console.log('Disconnecting from Socket.IO');
      socket.close();
    };
  }, []);

  const onStartRecording = () => {
    // elindítjuk, de 5 másodpercenként flusholunk
    recorder?.start(5000);
    console.log('Recording started');
    setRecording(true);
  };

  const onStopRecording = () => {
    recorder?.stop();
    console.log('Recording stopped');
    setRecording(false);
  };

  useEffect(() => {
    (async () => {
      if (!localVideoRef?.current) {
        return;
      }

      try {
        // elkérjük a webcam streamjét
        const stream = await navigator.mediaDevices.getUserMedia({
          audio: true,
          video: { width: 240 },
        });

        // a lokális stream bemeneteként állítjuk
        localVideoRef.current.srcObject = stream;

        // A Firefox nem támogatja a H264-et, ezért ellenőríznünk kell
        const mimeType = MediaRecorder.isTypeSupported('video/webm;codecs=h264')
          ? 'video/webm;codecs=h264'
          : 'video/webm';
        console.log(`Using codec "${mimeType}"`);

        // MediaRecorder elkészítése
        const newRecorder = new MediaRecorder(stream, {
          mimeType,
          videoBitsPerSecond: 200000,
        });

        newRecorder.ondataavailable = (event: BlobEvent) => {
          if (event.data.size > 0) {
            console.log(`Sending blob of size ${event.data.size}`);
            const blob = new Blob([event.data], { type: 'video/webm' });
            socket.emit('chunk', blob);

            if (newRecorder.state === 'inactive') {
              console.log('Apparently recording has been stopped, sending end signal');
              // lezárjuk a streamelést
              socket.disconnect();
            }
          }
        };

        newRecorder.onerror = console.error;
        setRecorder(newRecorder);
      } catch (err) {
        // eslint-disable-next-line no-alert
        alert(`Could not get devices. Did you give it permission? Is it open somewhere else?\n${err}`);
      }
    })();
  }, [localVideoRef]);

  return (
    <>
      <h1>MediaRecorder upload example</h1>

      <div>
        <b>Status: </b>
        <span id="status">{recording ? 'Recording...' : 'Ready to record'}</span>
      </div>

      <h2>Video</h2>

      <video className="local-video" ref={localVideoRef} autoPlay muted />

      <input type="button" value="Start" onClick={onStartRecording} disabled={recording} />
      <input type="button" value="Stop" onClick={onStopRecording} disabled={!recording} />

      <h2>Download files</h2>

      <div id="filenames">
        {filenames.map((filename) => (
          <div key={filename}>
            <a href={`/uploads/${filename}`}>{filename}</a>
          </div>
        ))}
      </div>
    </>
  );
}

const root = document.getElementById('root') as HTMLDivElement;
createRoot(root).render(<MediaRecorderUpload />);
