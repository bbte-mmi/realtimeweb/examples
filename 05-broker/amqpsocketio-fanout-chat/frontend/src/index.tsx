import { FormEvent, useCallback, useEffect, useMemo, useState } from 'react';
import { createRoot } from 'react-dom/client';
import { Socket, io } from 'socket.io-client';

import './style.css';

//
// socket io DTOs
//

type BroadcastMessage = {
  clientId: string;
  text: string;
  time: string;
};

interface ServerToClientEvents {
  message: (message: BroadcastMessage) => void;
  assignid: (id: string) => void;
}

interface ClientToServerEvents {
  message: (messageText: string) => void;
}

function ChatAmqpSocketIo({ socketIoUrl }: { socketIoUrl: string }) {
  const [clientId, setClientId] = useState('unknown');
  const [text, setText] = useState<string>('');
  const [messages, setMessages] = useState<Array<BroadcastMessage>>([]);

  // Kapcsolat megnyitása a dedikált WS felé
  const socket = useMemo<Socket<ServerToClientEvents, ClientToServerEvents>>(() => io(socketIoUrl), [socketIoUrl]);

  useEffect(() => {
    // ha üzenetet kapunk a szervertől
    socket.on('message', (message: BroadcastMessage) => {
      setMessages((m) => [...m, message]);
    });

    socket.on('assignid', (c: string) => setClientId(c));

    return () => {
      console.log('Disconnecting from WebSocket');
      socket.close();
    };
  }, []);

  // gombnyomás az üzenetnél
  const sendMessage = useCallback(
    (event: FormEvent) => {
      // szinkron leadás elkerülése
      event.preventDefault();

      // socket.io-val küldjük az üzenetet
      socket.emit('message', text);

      setText('');
    },
    [clientId, text, messages],
  );

  return (
    <>
      <div>
        <b>Status:</b> Connected to <code>{socketIoUrl}</code> as <code>{clientId}</code>
      </div>

      <div id="messages">
        {messages.map((message) => (
          <div key={`${message.clientId}-${message.time}`} className={message.clientId === clientId ? 'me' : 'them'}>
            <span className="name">{message.clientId}:&nbsp;</span>
            <span className="msg">{message.text}</span>
          </div>
        ))}
      </div>

      {/* form, amin keresztül üzenetet küldhetünk */}
      <form onSubmit={sendMessage}>
        <input
          style={{ textAlign: 'right' }}
          type="text"
          value={text}
          onChange={(event) => setText(event.target.value)}
          placeholder="Type message and press ENTER..."
        />
      </form>
    </>
  );
}

export function SocketIoUrlSetter() {
  const [socketIoUrl, setSocketIoUrl] = useState<string>('http://localhost:8080/');
  const [connected, setConnected] = useState<boolean>(false);

  const onConnect = useCallback((event: FormEvent) => {
    // szinkron leadás elkerülése
    event.preventDefault();
    setConnected(true);
  }, []);

  return (
    <>
      <h1>Scalable chat with socket.io + AMQP</h1>

      <form onSubmit={onConnect}>
        <input
          type="text"
          value={socketIoUrl}
          onChange={(event) => setSocketIoUrl(event.target.value)}
          disabled={connected}
        />
      </form>

      {connected && <ChatAmqpSocketIo socketIoUrl={socketIoUrl} />}
    </>
  );
}

const root = document.getElementById('root') as HTMLDivElement;
createRoot(root).render(<SocketIoUrlSetter />);
