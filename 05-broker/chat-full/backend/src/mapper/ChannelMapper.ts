import { ChannelCreationDto, ChannelDto } from '../dto/ChannelDto';
import Channel from '../model/Channel';

export function dtoToModel(dto: ChannelCreationDto): Partial<Channel> {
  return {
    name: dto.name,
  };
}

export function modelToDto(channel: Channel): ChannelDto {
  return {
    id: channel.id,
    name: channel.name,
  };
}

export function modelsToDtos(channels: Channel[]): ChannelDto[] {
  return channels.map(this.modelToDto);
}
