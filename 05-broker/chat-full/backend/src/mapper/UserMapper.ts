import { UserCreationDto, UserDto } from '../dto/UserDto';
import User from '../model/User';

export function dtoToModel(dto: UserCreationDto): Partial<User> {
  return {
    name: dto.name,
  };
}

export function modelToDto(user: User): UserDto {
  return {
    id: user.id,
    name: user.name,
  };
}

export function modelsToDtos(users: User[]): UserDto[] {
  return users.map(this.modelToDto);
}
