package edu.bbte.realtimeweb.amqpexample.config;

import edu.bbte.realtimeweb.amqpexample.errorhandling.MessageQueueErrorHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.boot.autoconfigure.amqp.SimpleRabbitListenerContainerFactoryConfigurer;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * RabbitMQ elérési paraméterek és konfiguráció
 * Konfigurálható propból vagy környezeti változóval a "rabbitmq." prefix-szel.
 */
@Configuration
@ConfigurationProperties(prefix = "rabbitmq")
@Slf4j
public class RabbitMQConfiguration {
    String host = "localhost";
    Integer port = 5672;
    String username = "guest";
    String password = "guest";
    String protocol = "amqp";

    /**
     * AMQP connection factory
     */
    @Bean
    public ConnectionFactory connectionFactory() {
        CachingConnectionFactory factory = new CachingConnectionFactory();
        factory.setUri(String.format("%s://%s:%d", protocol, host, port));
        factory.setUsername(username);
        factory.setPassword(password);
        return factory;
    }

    @Bean
    public SimpleRabbitListenerContainerFactory rabbitListenerContainerFactory(
            SimpleRabbitListenerContainerFactoryConfigurer configurer,
            MessageQueueErrorHandler errorHandler
    ) {
        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        configurer.configure(factory, connectionFactory());
        factory.setErrorHandler(errorHandler);
        return factory;
    }

    /**
     * Üzenetek (de)szerializációja történjen JackSONnel, i.e. JSON stílusú üzenetekkel dolgozunk
     */
    @Bean
    public MessageConverter messageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    /**
     * AMQP administratív operációs bean
     * Ezen keresztül hozhatunk létre sorokat/topic-okat.
     */
    @Bean
    public AmqpAdmin amqpAdmin() {
        var amqpAdmin = new RabbitAdmin(connectionFactory());

        // queue létrehozása
        // betehetnénk a listener "queuesToDeclare" kulcsába is
        Queue queue = QueueBuilder.durable("hello").build();
        amqpAdmin.declareQueue(queue);
        log.info("Successfully declared queue 'hello'");

        return amqpAdmin;
    }

    /**
     * RabbitMQ interakciós bean
     * Ezen keresztül küldhetünk üzeneteket.
     */
    @Bean
    public RabbitTemplate rabbitTemplate() {
        RabbitTemplate template = new RabbitTemplate(connectionFactory());
        template.setMessageConverter(messageConverter());
        return template;
    }
}
