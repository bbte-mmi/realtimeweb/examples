import { baseApiUrl } from '../constants';
import {
  BlogPostCreateDto,
  BlogPostDetailsDto,
  BlogPostDto,
  BlogPostSearchParams,
  BlogPostUpdateDto,
} from '../dto/blogpost.dto';

export const findAllBlogPosts = async (params: BlogPostSearchParams = {}): Promise<BlogPostDto[]> => {
  const response = await fetch(`${baseApiUrl}/blogPosts?${new URLSearchParams(params).toString()}`);
  const entities = (await response.json()) as BlogPostDto[];
  return entities;
};

export const findBlogPostById = async (blogPostId: string): Promise<BlogPostDetailsDto | undefined> => {
  const response = await fetch(`${baseApiUrl}/blogPosts/${blogPostId}`);
  if (response.status === 404) {
    return undefined;
  }
  const entity = (await response.json()) as BlogPostDetailsDto;
  return entity;
};

export const createBlogPost = async (blogPost: BlogPostCreateDto): Promise<BlogPostDetailsDto> => {
  const response = await fetch(`${baseApiUrl}/blogPosts`, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(blogPost),
  });

  if (response.status >= 400) {
    const { messages } = await response.json();
    throw new Error(messages || 'Unknown error');
  }

  const entity = (await response.json()) as BlogPostDetailsDto;
  return entity;
};

export const updateBlogPost = async (blogPostId: string, blogPost: BlogPostUpdateDto): Promise<BlogPostDetailsDto> => {
  const response = await fetch(`${baseApiUrl}/blogPosts/${blogPostId}`, {
    method: 'PATCH',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(blogPost),
  });

  if (response.status >= 400) {
    const { messages } = await response.json();
    throw new Error(messages || 'Unknown error');
  }

  const entity = (await response.json()) as BlogPostDetailsDto;
  return entity;
};

export const deleteBlogPost = async (blogPostId: string): Promise<void> => {
  await fetch(`${baseApiUrl}/blogPosts/${blogPostId}`, {
    method: 'DELETE',
  });
};
