import AmqpChannelProvider from './AmqpChannel';
import DataSourceProvider from './DataSource';
import repoProviders from './RepoProviders';

const providers = [...repoProviders, AmqpChannelProvider, DataSourceProvider];
export default providers;
