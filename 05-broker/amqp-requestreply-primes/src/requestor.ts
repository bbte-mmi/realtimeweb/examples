import * as amqp from 'amqplib';
import { randomBytes } from 'crypto';

//
// interakció DTO
//

type PrimeRequest = {
  number: number;
  replyQueue: string;
};

type PrimeReply = {
  number: number;
  result: boolean;
};

// random ID
const clientId = randomBytes(6).toString('base64');

const requestQueue = 'isprime';
const replyQueue = `isprimeresult-${clientId}`;

(async () => {
  const connection = await amqp.connect({});
  const channel = await connection.createChannel();

  await channel.assertQueue(requestQueue);
  await channel.assertQueue(replyQueue, { exclusive: true, autoDelete: true });

  channel.consume(replyQueue, (message) => {
    if (message) {
      const content = JSON.parse(message.content.toString()) as PrimeReply;
      console.log(`Response for number ${content.number}: ${content.result}`);
      channel.ack(message);
    }
  });

  let n = 1;

  const interval = setInterval(() => {
    console.log(`Sending number ${n} for prime check`);
    const request: PrimeRequest = {
      number: n,
      replyQueue,
    };
    const message = Buffer.from(JSON.stringify(request));
    channel.sendToQueue(requestQueue, message);
    // next mersienne number
    n = (n + 1) * 2 - 1;
  }, 1000);

  // SIGINT signal (Ctrl+C) esetén zárjuk a kapcsolatot
  process.once('SIGINT', async () => {
    console.log('Closing AMQP connection');
    clearInterval(interval);
    await channel.close();
    await connection.close();
  });
})();
