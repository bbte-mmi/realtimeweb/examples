import { FormEvent, useCallback, useEffect, useMemo, useState } from 'react';
import { createRoot } from 'react-dom/client';

import './style.css';

type Message = {
  text: string;
  sent: boolean;
  time: string;
};

function WebSocketEcho() {
  const [text, setText] = useState<string>('');
  const [messages, setMessages] = useState<Message[]>([]);

  // Kapcsolat megnyitása a dedikált WS felé
  const ws = useMemo(() => new WebSocket('wss://echo.websocket.events/'), []);

  useEffect(() => {
    // ha üzenetet kapunk a szervertől
    ws.onmessage = (event: MessageEvent<string>) => {
      console.log(event);
      const message: Message = {
        text: event.data,
        sent: false,
        time: new Date().toISOString(),
      };
      setMessages((m) => [...m, message]);
    };

    return () => {
      console.log('Disconnecting from WebSocket');
      ws.close();
    };
  }, []);

  // gombnyomás az üzenetnél
  const sendMessage = useCallback(
    (event: FormEvent) => {
      // szinkron leadás elkerülése
      event.preventDefault();

      // WebSockettel küldjük az üzenetet
      ws.send(text);

      // hozzáadjuk a listánkhoz
      const message: Message = {
        text,
        sent: true,
        time: new Date().toISOString(),
      };
      setMessages((m) => [...m, message]);
      // a szöveges bemenetet ürítjük
      setText('');
    },
    [text, messages],
  );

  return (
    <>
      <h1>WebSockets Echo Demo</h1>

      <div>
        <b>Status:</b> Connected as <code>abc</code>
      </div>

      <div id="messages">
        {messages.map((message) => (
          <div key={`${message.time}-${message.text}`} className={message.sent ? 'me' : 'them'}>
            <span className="name">{message.sent ? 'Me' : 'Echo Server'}:&nbsp;</span>
            <span className="msg">{message.text}</span>
          </div>
        ))}
      </div>

      {/* <!-- form, amin keresztül üzenetet küldhetünk --> */}
      <form onSubmit={sendMessage}>
        <input
          style={{ textAlign: 'right' }}
          type="text"
          value={text}
          onChange={(event) => setText(event.target.value)}
          placeholder="Type message and press ENTER..."
        />
      </form>
    </>
  );
}

const root = document.getElementById('root') as HTMLDivElement;
createRoot(root).render(<WebSocketEcho />);
