# Példaprogramokat elősegítő segédkörnyezet

- szükséges Docker

## RabbitMQ

- indítás: `make start-rabbit`
- leállítás: `make stop-rabbit`
- leállítás s adatok törlése: `make stop-cleanup-rabbit`

## Redis

- indítás: `make start-redis`
- leállítás: `make stop-redis`

## Mongo

- indítás: `make start-mongo`
- leállítás: `make stop-mongo`
- leállítás s adatok törlése: `make stop-cleanup-mongo`
